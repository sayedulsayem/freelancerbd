<?php ob_start(); ?>

<?php

$filepath= realpath(dirname(__FILE__));
include_once $filepath."/../admin/lib/session.php";

include_once $filepath."/../lib/user.php";

$login=session::get('login');
$loginID=session::get('id');
$user_type=session::get('user_type');

session::init();

?>


<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!--font-awesome-->
	<link rel="stylesheet" href="inc/css/font-awesome.min.css">
	<!--custom css-->
	<link rel="stylesheet" href="inc/css/style.css">

	<link rel="stylesheet" href="inc/css/freelancer-profile.css">

	<link rel="stylesheet" href="inc/css/job-post.css">

	<link rel="stylesheet" href="inc/css/jobs.css">
	<link rel="stylesheet" href="inc/css/index.css">
</head>
<body>

<?php

include_once "navbar.php";

?>

