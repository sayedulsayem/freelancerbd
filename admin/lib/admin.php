<?php

include_once "database.php";
include_once "session.php";

class admin{

	private $db;
	public function __construct()
	{
		$this->db=new database();
	}

	public function emailCheck($email){

		$email=$email;

		$sql="SELECT email FROM admins WHERE email=:email";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':email',$email);
		$query->execute();
		if($query->rowCount()>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	

	public function getLoginUser($email,$password){

		$sql= "SELECT * FROM `admins` WHERE email = :email AND password = :password LIMIT 1;";

		$query=$this->db->pdo->prepare($sql);

		$query->bindValue(':email',$email);

		$query->bindValue(':password',$password);

		$query->execute();

		$result=$query->fetch(PDO::FETCH_OBJ);

		return $result;

	}

	public function adminLogin($data){
		$email=$data['email'];
		$pass=$data['password'];
		$password=md5($pass);

		$chk_email=$this->emailCheck($email);

		if($email=="" OR $pass== ""){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Field must not be empty</div>";
			return $msg;
		}
		if (filter_var($email, FILTER_VALIDATE_EMAIL)=== false){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Enter a valid email address</div>";
			return $msg;
		}
		if ($chk_email==false){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> This email address does not exist as admin </div>";
			return $msg;
		}

		$result= $this->getLoginUser($email,$password);

		if ($result){

			session::init();
			session::set("login",true);
			session::set("id",$result->id);
			session::set("user_name",$result->user_name);
			session::set("email",$result->email);
			session::set("user_type","admin");
			session::set("loginmsg","<div class='alert alert-success'><strong>Success !</strong> You are loggedIn</div>");

			$msg="<div class='alert alert-success'><strong>Success !</strong> thank you, you have successfully logged in</div>";
			return $msg;
		}
		else{
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Email and password doesn't match</div>";
			return $msg;
		}

	}

	public function getAdminDataByID($reqID){
		$userId=$reqID;
		$sql= "SELECT * FROM `admins` WHERE id= :id LIMIT 1";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':id',$userId);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function add_category($data){

		$cat_name=$data['cat_name'];
		$cat_desc=$data['cat_desc'];

		$sql="INSERT INTO `categories` (`cat_name`, `cat_desc`) VALUES (:cat_name, :cat_desc);";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':cat_name',$cat_name);
		$query->bindValue('cat_desc',$cat_desc);
		$query->execute();

		$msg="<div class='alert alert-success'><strong>Success !</strong> Category added</div>";
		return $msg;

	}

	public function getCatList(){

		$sql="SELECT * FROM `categories` ORDER BY id DESC;";
		$query=$this->db->pdo->prepare($sql);
		$query->execute();
		$result=$query->fetchAll();

		return $result;

	}

	public function addSkill($data){
		$sk_name=$data["sk_name"];
		$cat_id=$data['cat_id'];
		$sk_desc=$data['sk_desc'];

		$sql="INSERT INTO `skilles` (`sk_name`, `cat_id`, `sk_desc`) VALUES (:sk_name, :cat_id, :sk_desc);";

		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':sk_name',$sk_name);
		$query->bindValue(':cat_id',$cat_id);
		$query->bindValue(':sk_desc',$sk_desc);
		$query->execute();

		$msg="<div class='alert alert-success'><strong>Success !</strong> Skill added</div>";
		return $msg;

	}

	public function getSkillList(){

		$sql="SELECT skilles.id,cat_id,cat_name,sk_name,sk_desc FROM `skilles`,`categories` WHERE skilles.cat_id=categories.id ORDER BY skilles
.id DESC;";
		$query=$this->db->pdo->prepare($sql);
		$query->execute();
		$result=$query->fetchAll();

		return $result;

	}


}
?>