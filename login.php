<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');
session::checkLogin();

?>
<?php

$user=new user();

if ($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['login'])){
    $userLogin=$user->userLogin($_POST);
}

?>


                                    <!--carousel starts here-->
<section id="header-login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2> <i class="fa fa-coffee" aria-hidden="true"></i> Login</h2>
                <h4>Log in and get to work.</h4>
            </div>
        </div>
    </div>
</section>
                                    <!--carousel ends here-->
                                    <!--Signup starts here-->
<section id="signup">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center col-lg-offset-6">
                <h3 class="login-head">Login</h3>
	            <?php
	            if (isset($userLogin)){
		            $user_type=session::get('user_type');
		            $login=session::get('login');
	                echo $userLogin;
	                if ($user_type=="freelancer"){
	                    header('Location:freelancer_profile.php');
                    }
                    elseif ($user_type=="client"){
	                    header('Location:client_profile.php');
                    }
	            }

	            ?>
                <form action="" method="post">

                  <div class="form-group">
                    <input style="display: block; position:relative;" type="email" name="email" class="form-control custom-form-login" id="exampleInputEmail1" placeholder="Enter email">
                  </div>

                  <div class="form-group">
                    <input style="display: block; position: relative;" type="password" name="password" class="form-control custom-form-login" id="exampleInputPassword1" placeholder="Password">
                  </div>

                  <button name="login" type="submit" class="btn login-button">Login</button>

                </form>

                <a class="login-form-a" href="">Forgot Password?</a>
                
            </div>

        </div>
    </div>
</section>
                                    <!--Signup ends here-->


<?php
require_once "inc/footer.php";
?>