<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');

?>

<!--carousel starts here-->
<section id="header-login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="btn-headline"> <i class="fa fa-tag" aria-hidden="true"></i> Post a job, It's free</h2>
            </div>
        </div>
    </div>
</section>
<!--carousel ends here-->
<!--Jobs starts here-->
<section id="jobs">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-4">

                <div class="list">
                    <div class="list-group">
                        <span class="list-group-item active cat-top">
                            <em class="fa fa-fw fa-coffee text-white">Categories
                                <span class="badge active">200+</span>
                            </em>
                        </span>
                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>


                    </div>
                </div>

                <div class="list">
                    <div class="list-group">
                        <span class="list-group-item active cat-top">
                            <em class="fa fa-fw fa-coffee text-white">Categories
                                <span class="badge active">200+</span>
                            </em>
                        </span>
                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>
                    </div>
                </div>


                <div class="list">
                    <div class="list-group">
                        <span class="list-group-item active cat-top">
                            <em class="fa fa-fw fa-coffee text-white">Categories
                                <span class="badge active">200+</span>
                            </em>
                        </span>
                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>

                        <a class="list-group-item cat-list" href="">
                            <em class="fa fa-fw fa-code text-muted">
                            </em>
                            Web Developement & IT
                            <span class="badge text-red-bg">20+</span>
                        </a>
                    </div>
                </div>



            </div>

            <div class="col-xs-12 col-lg-8">
                <form action="" class="list-search">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">

                        </div>
                        <input type="text" class="form-control " id="inlineFormInputGroup" placeholder="Job title, Keywords or Company Name">
                        <div class="button input-group-font"><i class="fa fa-search"></i></div>
                    </div>
                </form>

                <h6 class="search-headline">We found 100 jobs matching: Web Development & IT</h6>

                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="">
                                    <img width="50" class="img-responsive" src="img/banner.jpg" alt="">
                                </a>
                            </div>

                            <div class="col-lg-10 col-xs-12">
                                <h4><a href="">
                                    Code Ebay store and listing design to be mobile responsive.</a></h4>
                                <h5>John Doe <small>@JohnDoe</small></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                        </div>
                    </div>

                    <div class="row bottom-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-4">
                                <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                    <i class="fa fa-align-left">Send Proposal</i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <!--second part-->

                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="">
                                    <img width="50" class="img-responsive" src="img/banner.jpg" alt="">
                                </a>
                            </div>

                            <div class="col-lg-10 col-xs-12">
                                <h4><a href="">
                                    Code Ebay store and listing design to be mobile responsive.</a></h4>
                                <h5>John Doe <small>@JohnDoe</small></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                        </div>
                    </div>

                    <div class="row bottom-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-4">
                                <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                    <i class="fa fa-align-left">Send Proposal</i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <!--third part-->

                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="">
                                    <img width="50" class="img-responsive" src="img/banner.jpg" alt="">
                                </a>
                            </div>

                            <div class="col-lg-10 col-xs-12">
                                <h4><a href="">
                                    Code Ebay store and listing design to be mobile responsive.</a></h4>
                                <h5>John Doe <small>@JohnDoe</small></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                        </div>
                    </div>

                    <div class="row bottom-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-4">
                                <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                    <i class="fa fa-align-left">Send Proposal</i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <!--fourth part-->

                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="">
                                    <img width="50" class="img-responsive" src="img/banner.jpg" alt="">
                                </a>
                            </div>

                            <div class="col-lg-10 col-xs-12">
                                <h4><a href="">
                                    Code Ebay store and listing design to be mobile responsive.</a></h4>
                                <h5>John Doe <small>@JohnDoe</small></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                        </div>
                    </div>

                    <div class="row bottom-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-4">
                                <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                    <i class="fa fa-align-left">Send Proposal</i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>




                <div class="page text-center">
                    <ul class="pagination">
                        <li class="disabled"><a href=""><<</a></li>
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                        <li><a href="">>></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>
<!--Jobs ends here-->




<?php
require_once "inc/footer.php";
?>