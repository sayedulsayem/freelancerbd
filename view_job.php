<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');

?>


<!--carousel starts here-->
<section id="header-login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="btn-headline"> <i class="fa fa-star-o" aria-hidden="true"></i> Bookmark This Job</h2>
            </div>
        </div>
    </div>
</section>
<!--carousel ends here-->
<!--JobsPost starts here-->
<section id="jobs-post" class="jobpost">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 white">
                <div class="row">
                    <div class="col-lg-12">
                        <h6><a href="">Categories/Webdesign & IT</a></h6>
                        <h4>Code ebay store here is starts and ends</h4>
                        <hr class="small-hr">

                    </div>
                </div>

                <div class="row post-top-sec">
                    <div class="col-lg-3">
                        <h5>Posted</h5>
                        <p>4 hours ago</p>
                    </div>
                    <div class="col-lg-3">
                        <h5>Posted</h5>
                        <p>4 hours ago</p>
                    </div>
                    <div class="col-lg-3">
                        <h5>Posted</h5>
                        <p>4 hours ago</p>
                    </div>
                    <div class="col-lg-3">
                        <h5>Posted</h5>
                        <p>4 hours ago</p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <hr class="small-hr">
                </div>

                <div class="post-bottom-sec">
                    <h4>This is headline</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam aut deserunt dicta doloremque ducimus excepturi illum, incidunt ipsa libero nam odit officia, pariatur praesentium quidem quo saepe sapiente sed.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam aut deserunt dicta doloremque ducimus excepturi illum, incidunt ipsa libero nam odit officia, pariatur praesentium quidem quo saepe sapiente sed.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam aut deserunt dicta doloremque ducimus excepturi illum, incidunt ipsa libero nam odit officia, pariatur praesentium quidem quo saepe sapiente sed.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam aut deserunt dicta doloremque ducimus excepturi illum, incidunt ipsa libero nam odit officia, pariatur praesentium quidem quo saepe sapiente sed.</p>
                    <h4>Responsibilities</h4>
                    <ul class="square">
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                    </ul>

                    <h4>Job Requirement</h4>
                    <ul class="square">
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                        <li>Pro munere assueverit id, debitis scaevola omittantur vim ex, qui meis tantas et. Vivendo ponderum id pro!</li>
                    </ul>

                    <h4>Skills</h4>
                    <span class="label label-success">HTML5</span>
                    <span class="label label-success">HTML5</span>
                    <span class="label label-success">HTML5</span>
                    <span class="label label-success">HTML5</span>
                    <span class="label label-success">HTML5</span>
                </div>



                <h4>Proposals</h4>
                <div class="job">
                <div class="row top-sec">
                    <div class="col-lg-12">
                        <div class="col-lg-2 col-xs-12">
                            <a href="">
                                <img width="50" class="img-responsive" src="inc/img/banner.jpg" alt="">
                            </a>
                        </div>

                        <div class="col-lg-10 col-xs-12">
                            <h4><a href="">
                                Code Ebay store and listing design to be mobile responsive.</a></h4>
                            <h5>John Doe <small>@JohnDoe</small></h5>
                        </div>
                    </div>
                </div>

                <div class="row mid-sec">
                    <div class="col-lg-12">
                        <hr class="small-hr">
                        <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                        <span class="label label-success">HTML5</span>
                        <span class="label label-success">HTML5</span>
                        <span class="label label-success">HTML5</span>
                        <span class="label label-success">HTML5</span>
                        <span class="label label-success">HTML5</span>
                    </div>
                </div>

                <div class="row bottom-sec">
                    <div class="col-lg-12">
                        <hr class="small-hr">
                        <div class="col-lg-2">
                            <h5>Headline</h5>
                            <p>Paragraph</p>
                        </div>
                        <div class="col-lg-2">
                            <h5>Headline</h5>
                            <p>Paragraph</p>
                        </div>
                        <div class="col-lg-2">
                            <h5>Headline</h5>
                            <p>Paragraph</p>
                        </div>
                        <div class="col-lg-2">
                            <h5>Headline</h5>
                            <p>Paragraph</p>
                        </div>
                        <div class="col-lg-4">
                            <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                <i class="fa fa-align-left">View Proposal</i>
                            </a>
                        </div>
                    </div>
                </div>



                <div class="job">
                    <div class="row top-sec">
                        <div class="col-lg-12">
                            <div class="col-lg-2 col-xs-12">
                                <a href="">
                                    <img width="50" class="img-responsive" src="inc/img/banner.jpg" alt="">
                                </a>
                            </div>

                            <div class="col-lg-10 col-xs-12">
                                <h4><a href="">
                                    Code Ebay store and listing design to be mobile responsive.</a></h4>
                                <h5>John Doe <small>@JohnDoe</small></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mid-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <p>Description of every page/module: I have a PSD ebay store and listing design in photoshop that needs to be sliced and coded for eBay to be mobile responsive. Description of requirements/features: Mobile Responsive Ebay store and listing design...</p>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                            <span class="label label-success">HTML5</span>
                        </div>
                    </div>

                    <div class="row bottom-sec">
                        <div class="col-lg-12">
                            <hr class="small-hr">
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-2">
                                <h5>Headline</h5>
                                <p>Paragraph</p>
                            </div>
                            <div class="col-lg-4">
                                <a class="frlncr-btn frlncr-btn-mint-small" href="">
                                    <i class="fa fa-align-left">View Proposal</i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>






                <div class="page text-center">
                    <ul class="pagination">
                        <li class="disabled"><a href=""><<</a></li>
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                        <li><a href="">>></a></li>
                    </ul>
                </div>

        </div>
            <div class="col-lg-4">
                <div class="panel user">
                    <div class="row text-center">
                        <a href="">
                            <img class="cover-img" src="inc/img/banner.jpg" height="150" alt="">
                            <div class="col-xs-12 user avatar">
                                <img src="inc/img/banner.jpg" height="90" width="90" class="img-thumbnail avatar" alt="">
                                <h4>John Doe</h4>
                                <p>@johndoe</p>
                            </div>
                        </a>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            Paid
                            <span class="badge">
                                 $10k
                             </span>

                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            Ratting
                            <span class="badge">
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                             </span>

                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            Unpaid Invoice
                            <span class="badge">
                                 2
                             </span>

                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            Paid Job
                            <span class="badge">
                                 50
                             </span>

                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            Ammount Paid Job
                            <span class="badge">
                                 $105k
                             </span>

                        </div>
                    </div>

                    <div class="list">
                        <div class="list-group">
                        <span class="list-group-item active cat-top">
                            <em class="fa fa-fw fa-coffee text-white">
                            </em>
                            Share This Job
                        </span>
                            <a class="list-group-item cat-list" href="">
                                <em class="fa fa-facebook text-muted">
                                </em>
                                Facebook
                                <span class="badge text-red-bg">20+</span>
                            </a>

                            <a class="list-group-item cat-list" href="">
                                <em class="fa fa-fw fa-twitter text-muted">
                                </em>
                                Twitter
                                <span class="badge text-red-bg">20+</span>
                            </a>

                            <a class="list-group-item cat-list" href="">
                                <em class="fa fa-fw fa-google text-muted">
                                </em>
                                Google
                                <span class="badge text-red-bg">20+</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
</section>

<!--JobsPost ends here-->


<?php
require_once "inc/footer.php";
?>