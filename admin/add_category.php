<?php
require_once "dist/header.php";

include_once "lib/admin.php";
session::checkSession();

$admin=new admin();

if ($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['cat_submit'])){
    $add_cat=$admin->add_category($_POST);
}

?>

	<div class="content-wrapper">
		<section class="content">

            <?php
            if (isset($add_cat)){
                echo $add_cat;
            }
            ?>

            <h1>Add New Category</h1>

            <form role="form" action="" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="cat_name">Category Name</label>
                        <input name="cat_name" type="text" class="form-control" id="cat_name" placeholder="Enter a category name">
                    </div>
                    <div class="form-group">
                        <label style="display: block" for="cat_desc">Category Description</label>
                        <textarea name="cat_desc" id="cat_desc" cols="187" rows="10"></textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <button name="cat_submit" type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

		</section>
	</div>

<?php
require_once "dist/footer.php";
?>