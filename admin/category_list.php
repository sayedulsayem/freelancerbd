<?php
require_once "dist/header.php";
include_once "lib/admin.php";
session::checkSession();

$admin=new admin();

$cat_list=$admin->getCatList();

?>

	<div class="content-wrapper">
		<section class="content">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Category List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Category ID</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                        </tr>
                        </thead>
	                    <?php
	                    if (isset($cat_list)){
		                    foreach ($cat_list as $item){
			                    ?>
                                <tbody>
                                <tr>
                                    <td><?php echo $item['id']; ?></td>
                                    <td><?php echo $item['cat_name']; ?></td>
                                    <td><?php echo $item['cat_desc']; ?></td>
                                </tr>
                                </tbody>
			                    <?php
		                    }
	                    }
	                    ?>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

		</section>
	</div>

<?php
require_once "dist/footer.php";
?>