<?php

$filepath= realpath(dirname(__FILE__));

include_once $filepath."/../lib/session.php";
include_once $filepath."/../lib/admin.php";

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);

?>

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
	<li class="header">Access</li>
	<li class="<?php echo $active = ($basename == 'index') ? ' active' : ''; ?>">
		<a href="index.php">
			<i class="fa fa-dashboard"></i>Dashboard
		</a>
	</li>
	<li class="treeview <?php echo $active = ($basename == 'admins_list' || $basename == 'add_new_admin') ? ' active' : ''; ?>">
		<a href="#">
			<i class="fa fa-user"></i>
			<span>Admins</span>
			<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
		</a>
		<ul class="treeview-menu">
			<li class="<?php echo $active = ($basename == 'admins_list') ? ' active' : ''; ?>"><a href="admins_list.php"><i class="fa fa-circle-o"></i> Admins List</a></li>
			<li class="<?php echo $active = ($basename == 'add_new_admin') ? ' active' : ''; ?>"><a href="add_new_admin.php"><i class="fa fa-circle-o"></i> Add New Admin</a></li>
		</ul>
	</li>
	<li class="header">Accounts</li>
	<li class="treeview <?php echo $active = ($basename == 'freelancer_list' || $basename == 'add_freelancer') ? ' active' : ''; ?>">
		<a href="#">
			<i class="fa fa-users"></i>
			<span>Freelancers</span>
			<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
		</a>
		<ul class="treeview-menu">
			<li class="<?php echo $active = ($basename == 'freelancer_list') ? ' active' : ''; ?>"><a href="freelancer_list.php"><i class="fa fa-circle-o"></i> Freelancers list</a></li>
			<li class="<?php echo $active = ($basename == 'add_freelancer') ? ' active' : ''; ?>"><a href="add_freelancer.php"><i class="fa fa-circle-o"></i> Add new freelancer</a></li>
		</ul>
	</li>
	<li class="treeview <?php echo $active = ($basename == 'client_list' || $basename == 'add_client') ? ' active' : ''; ?>">
		<a href="#">
			<i class="fa fa-user-md"></i>
			<span>Clients</span>
			<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
		</a>
		<ul class="treeview-menu">
			<li class="<?php echo $active = ($basename == 'client_list') ? ' active' : ''; ?>"><a href="client_list.php"><i class="fa fa-circle-o"></i> Clients list</a></li>
			<li class="<?php echo $active = ($basename == 'add_client') ? ' active' : ''; ?>"><a href="add_client.php"><i class="fa fa-circle-o"></i> Add new Client</a></li>
		</ul>
	</li>
	<li class="header">Management</li>

	<li class="treeview <?php echo $active = ($basename == 'category_list' || $basename == 'add_category') ? ' active' : ''; ?>">
		<a href="#">
			<i class="fa fa-align-left"></i>
			<span>Categories</span>
			<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
		</a>
		<ul class="treeview-menu">
			<li class="<?php echo $active = ($basename == 'category_list') ? ' active' : ''; ?>"><a href="category_list.php"><i class="fa fa-circle-o"></i> Category List</a></li>
			<li class="<?php echo $active = ($basename == 'add_category') ? ' active' : ''; ?>"><a href="add_category.php"><i class="fa fa-circle-o"></i> Add Category</a></li>
		</ul>
	</li>
	<li class="treeview <?php echo $active = ($basename == 'skill_list' || $basename == 'add_skill') ? ' active' : ''; ?>">
		<a href="#">
			<i class="fa fa-cogs"></i>
			<span>Skills</span>
			<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
		</a>
		<ul class="treeview-menu">
			<li class="<?php echo $active = ($basename == 'skill_list') ? ' active' : ''; ?>"><a href="skill_list.php"><i class="fa fa-circle-o"></i> Skill list</a></li>
			<li class="<?php echo $active = ($basename == 'add_skill') ? ' active' : ''; ?>"><a href="add_skill.php"><i class="fa fa-circle-o"></i> Add new Skill</a></li>
		</ul>
	</li>

	<li class="<?php echo $active = ($basename == 'jobs_list') ? ' active' : ''; ?>">
		<a href="jobs_list.php">
			<i class="fa fa-align-left"></i> <span>Jobs List</span>
		</a>
	</li>
	<li class="<?php echo $active = ($basename == 'jobs_invitation') ? ' active' : ''; ?>">
		<a href="jobs_invitation.php">
			<i class="fa fa-filter"></i> <span>Jobs Invites</span>
		</a>
	</li>
	<li class="<?php echo $active = ($basename == 'proposals') ? ' active' : ''; ?>">
		<a href="proposals.php">
			<i class="fa fa-files-o"></i> <span>Proposals</span>
		</a>
	</li>
	<li class="<?php echo $active = ($basename == 'job_assigned') ? ' active' : ''; ?>">
		<a href="job_assigned.php">
			<i class="fa fa-address-card"></i> <span>Job Assigned</span>
		</a>
	</li>

</ul>