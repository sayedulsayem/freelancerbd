-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2018 at 11:02 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freelancerbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `division` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `sub_dist` varchar(255) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `house_no` int(11) NOT NULL,
  `road_no` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `mobile`, `division`, `district`, `sub_dist`, `zipcode`, `house_no`, `road_no`) VALUES
(1, 564198, 'fgjhj', 'dfjdj', 'djgj', 6589, 65489, 'fsdjnfd'),
(2, 123456, 'dhaka', 'dhaka', 'dhaka', 0, 25, '25'),
(5, 123, 'dhaka', 'dhaka', 'dhaka', 0, 25, '14'),
(6, 1234, 'dhaka', 'dhaka', 'dhaka', 0, 52, '52'),
(8, 12345, 'dhaka', 'dhaka', 'dhaka', 0, 12, '12'),
(9, 156, 'dhaka', 'dhaka', 'dhaka', 0, 15, '15'),
(10, 452411, 'dhaka', 'dhaka', 'dhaka', 0, 154, '154'),
(11, 123456789, 'dhaka', 'dhaka', 'dhaka', 0, 45, '45'),
(12, 11111111, 'dhaka', 'dhaka', 'dhaka', 0, 111, '01111'),
(13, 1245, 'rajshahi', 'manikganj', 'dhaka', 0, 12, '45');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `user_name`, `email`, `password`) VALUES
(1, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_desc`) VALUES
(1, 'web developer', 'web developer'),
(2, 'web desiner', 'web desiner');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `dist_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `dist_name`) VALUES
(1, 1, 'Dhaka'),
(2, 1, 'Manikganj'),
(3, 1, 'Gazipur'),
(4, 1, 'Gopalganj'),
(5, 1, 'Kishoreganj'),
(6, 1, 'Madaripur'),
(7, 1, 'Munshiganj'),
(8, 1, 'Narayanganj'),
(9, 1, 'Rajbar'),
(10, 1, 'Shariatpur'),
(11, 1, 'Faridpur'),
(12, 1, 'Tangail'),
(13, 1, 'Narsingdi'),
(14, 2, 'Chattogram'),
(15, 2, 'Bandarban'),
(16, 2, 'Brahmanbaria'),
(17, 2, 'Chandpur'),
(18, 2, 'Cumilla'),
(19, 2, 'Cox\'s Bazar'),
(20, 2, 'Feni'),
(21, 2, 'Khagrachhari'),
(22, 2, 'Lakshmipur'),
(23, 2, 'Noakhali'),
(24, 2, 'Rangamati'),
(25, 3, 'Rajshahi'),
(26, 3, 'Joypurhat'),
(27, 3, 'Bogura'),
(28, 3, 'Naogaon'),
(29, 3, 'Natore'),
(30, 3, 'Nawabganj'),
(31, 3, 'Pabna'),
(32, 3, 'Sirajganj'),
(33, 4, 'Khulna'),
(34, 4, 'Bagerhat'),
(35, 4, 'Chuadanga'),
(36, 4, 'Jashore'),
(37, 4, 'Jhenaida'),
(38, 4, 'Kushtia'),
(39, 4, 'Magura'),
(40, 4, 'Meherpur'),
(41, 4, 'Narail'),
(42, 4, 'Satkhira'),
(43, 5, 'Rangpur'),
(44, 5, 'Dinajpur'),
(45, 5, 'Gaibandha'),
(46, 5, 'Kurigram'),
(47, 5, 'Lalmonirhat'),
(48, 5, 'Nilphamari'),
(49, 5, 'Panchagarh'),
(50, 5, 'Thakurgaon'),
(51, 6, 'Sylhet'),
(52, 6, 'Habiganj'),
(53, 6, 'Moulvibazar'),
(54, 6, 'Sunamganj'),
(55, 7, 'Barishal'),
(56, 7, 'Barguna'),
(57, 7, 'Bhola'),
(58, 7, 'Jhalokati'),
(59, 7, 'Patuakhali'),
(60, 7, 'Pirojpur'),
(61, 8, 'Mymensingh'),
(62, 8, 'Jamalpur'),
(63, 8, 'Netrokona'),
(64, 8, 'Sherpur');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(11) NOT NULL,
  `division_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `division_name`) VALUES
(1, 'Dhaka'),
(2, 'Chattogram'),
(3, 'Rajshahi'),
(4, 'Khulna'),
(5, 'Rangpur'),
(6, 'Sylhet'),
(7, 'Barishal'),
(8, 'Mymensingh');

-- --------------------------------------------------------

--
-- Table structure for table `freelancer_categories`
--

CREATE TABLE `freelancer_categories` (
  `id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `freelancer_proposals`
--

CREATE TABLE `freelancer_proposals` (
  `id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `offer_payment` int(11) NOT NULL,
  `cover_letter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `freelancer_skills`
--

CREATE TABLE `freelancer_skills` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freelancer_skills`
--

INSERT INTO `freelancer_skills` (`id`, `user_id`, `sk_id`) VALUES
(1, 10, 1),
(2, 13, 2),
(3, 13, 1),
(4, 13, 3),
(5, 13, 4),
(6, 13, 5),
(7, 13, 6);

-- --------------------------------------------------------

--
-- Table structure for table `job_posts`
--

CREATE TABLE `job_posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sk_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `job_desc` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_posts`
--

INSERT INTO `job_posts` (`id`, `user_id`, `cat_id`, `sk_id`, `job_title`, `job_desc`, `payment`, `date`, `start_time`, `end_time`) VALUES
(2, 14, 2, 2, 'this is job', 'sghsdfh', 'dfshdgh', '2018-04-18', '13:00:00', '15:00:00'),
(3, 14, 1, 2, 'PSD to HTML', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', '$ 50', '2018-04-18', '10:00:00', '23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job_proposals`
--

CREATE TABLE `job_proposals` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `proposal_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cover_letter` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `msg` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `by_user` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skilles`
--

CREATE TABLE `skilles` (
  `id` int(11) NOT NULL,
  `sk_name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sk_desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skilles`
--

INSERT INTO `skilles` (`id`, `sk_name`, `cat_id`, `sk_desc`) VALUES
(1, 'html', 2, 'html'),
(2, 'php', 1, 'php'),
(3, 'css', 2, ''),
(4, 'javascript', 2, ''),
(5, 'mysql', 1, ''),
(6, 'python', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sub_districts`
--

CREATE TABLE `sub_districts` (
  `id` int(11) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `sub_dist_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL,
  `about` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `mobile`, `about`, `user_type`) VALUES
(1, 'sayem', 'sayedul', 'sayem', 'sayem@gmail.com', 'sayem', 1715, 'dgh asdklf ghdfsl gdsfg hsd gh', 'freelancer'),
(2, 'sayem', 'sayedul', 'sayem', 'sayedulsayem@gmail.com', 'ea9c60305c58d8c8b81432bdc45cc472', 65481, 'fgjhdthdgt', 'freelancer'),
(3, 'sayedulsayem', 'sayedul', 'sayem', 'sayedulsayem2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 123456, 'sadfghsdfgh', 'client'),
(6, 'user', 'user f', 'user f', 'user@gmail.com', '6ad14ba9986e3615423dfca256d04e3f', 123, 'sdfg', 'freelancer'),
(7, 'user1', 'user1', 'user1', 'user1@gmail.com', '6ad14ba9986e3615423dfca256d04e3f', 1234, 'user1', 'freelancer'),
(9, 'user2', 'user2', 'user2', 'user2@gmail.com', '6ad14ba9986e3615423dfca256d04e3f', 12345, 'rthy', 'freelancer'),
(10, 'user2', 'user2', 'user2', 'user2c@gmail.com', '6ad14ba9986e3615423dfca256d04e3f', 156, 'uj', 'client'),
(11, 'client', 'client', 'client', 'client1@gmail.com', '3677b23baa08f74c28aba07f0cb6554e', 452411, 'dzsfh', 'client'),
(13, 'admin', 'admin', 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 11111111, 'admin', 'freelancer'),
(14, 'client', 'client', 'client', 'client@gmail.com', '62608e08adc29a8d6dbc9754e659f125', 1245, 'sweryhhsfdthd', 'client');

-- --------------------------------------------------------

--
-- Table structure for table `zip_code`
--

CREATE TABLE `zip_code` (
  `id` int(11) NOT NULL,
  `sub_dist_id` int(11) NOT NULL,
  `zipcode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freelancer_categories`
--
ALTER TABLE `freelancer_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freelancer_proposals`
--
ALTER TABLE `freelancer_proposals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freelancer_skills`
--
ALTER TABLE `freelancer_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_posts`
--
ALTER TABLE `job_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_proposals`
--
ALTER TABLE `job_proposals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skilles`
--
ALTER TABLE `skilles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_districts`
--
ALTER TABLE `sub_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `zip_code`
--
ALTER TABLE `zip_code`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `freelancer_categories`
--
ALTER TABLE `freelancer_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `freelancer_proposals`
--
ALTER TABLE `freelancer_proposals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `freelancer_skills`
--
ALTER TABLE `freelancer_skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `job_posts`
--
ALTER TABLE `job_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_proposals`
--
ALTER TABLE `job_proposals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skilles`
--
ALTER TABLE `skilles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_districts`
--
ALTER TABLE `sub_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `zip_code`
--
ALTER TABLE `zip_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
