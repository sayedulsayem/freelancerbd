<!--Footer starts here-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<h3>About Us</h3>
				<hr class="mint2">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore.</p>
			</div>

			<div class="col-xs-12 col-sm-3 ">
				<h3>Company</h3>
				<hr class="mint2">
				<ul class="footer-ul">
					<li class="footer-li"><a href="">Home</a></li>
					<li class="footer-li"><a href="">About</a></li>
					<li class="footer-li"><a href="">Jobs</a></li>
					<li class="footer-li"><a href="">Freelancers</a></li>
					<li class="footer-li"><a href="">Profiles</a></li>
					<li class="footer-li"><a href="">Contact</a></li>
					<li class="footer-li"><a href="">FAQ</a></li>
				</ul>
			</div>

			<div class="col-xs-12 col-sm-3">
				<h3>Other Service</h3>
				<hr class="mint2">
				<ul>
					<li>Privacy Policy</li>
					<li>Terms of use</li>
					<li>FAQ</li>
				</ul>
			</div>

			<div class="col-xs-12 col-sm-3">
				<h3>Browse</h3>
				<hr class="mint2">
				<ul>
					<li>Top Freelancers this Month of June</li>
					<li>Top Freelancers by Skill</li>
					<li>Top Freelancers in USA</li>
					<li>Top Freelancers in UK</li>
					<li>Top Freelancers in Australia</li>
					<li>Top Freelancers in Kenya</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<footer id="last-footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<ul class="footer-ul social-links">
					<li class="footer-li"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li class="footer-li"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li class="footer-li"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li class="footer-li"><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-md-4 footer-brand-icon">
				<a class="footer-brand" href="#">
					<i class="fa fa-coffee footer-icon" aria-hidden="true"></i>
				</a>
			</div>

			<div class="col-xs-12 col-md-4">
				<p>©2018 FreelancerBD. Template by EU FreelancerBD</p>
			</div>

		</div>
	</div>
</footer>
<!--Footer ends here-->








<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script type="text/javascript" src="inc/js/functions.js"></script>

<script type="text/javascript">
   $(document).ready(function () {
       var scroll_start = 0;
       var start_change = $('#signup');
       var offset = start_change.offset();

       if (start_change.length){
           $(document).scroll(function () {
               scroll_start = $(this).scrollTo();

               if (scroll_start > offset.top){
                   $('.nav-bg-color').css('background-color','rgba(0,64,123,0.9)!important'));
               }
               else {
                   $('.nav-bg-color').css('background-color','transparent!important');
       }
           })
       }
   })


</script>
</body>
</html>
</body>
</html>