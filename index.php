<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');
$loginID=session::get('id');
$user_type=session::get('user_type');

?>

<!-- ==============================================
	 Header
	 =============================================== -->
     <section id="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h1><i class="fa fa-cup"></i>The Freelancer BD</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
            <h3>Hire the best freelancers for your work.</h3>
            <a href="client_registration.php"><button type="button" class="btn btn-success">Hire</button></a>
            <a href="freelancer_registration.php"><button type="button" class="btn btn-success">Work</button></a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                
            </div>

           

            <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12 text-center">
               
            </div>

        </div>
    </div>
</section>

<?php
//if ($user_type=="admin"){
//    $login=false;
//    $loginID=false;
//    $user_type=false;
//}
//?>

<!--<h1>--><?php //var_dump($login); ?><!--</h1>-->
<!--<h1>--><?php //var_dump($loginID); ?><!--</h1>-->
<!--<h1>--><?php //var_dump($user_type); ?><!--</h1>-->

<!-- ==============================================
Freelance Services Section
=============================================== -->
<section id="services" class="services">
    <div class="container text-center">

        <div class="row">
            <div class="col-lg-12">
                <h3>Browse Freelance Services</h3>
                <hr class="mint">
                <p class="top-p">View over 30,000 available services by category.</p>

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html" class="hover">
                        <div class="features one">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-code fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Web Development & IT</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Designer, Developer, Project Management, Front-End Developer.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features two">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-eye fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Design, Art & Multimedia</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Design, Logo Design, Animation, Graphic Design, 3D, Adobe photoshop.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features three">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Writing & Translation</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Content Writing, English Translation, Articles, SEO, Translation, Research.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features four">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-cogs fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Admin Support</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Data Entry, English, Excel, Customer Service, Admin Support, Research, Email.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features five">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-table fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Management & Finance</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Finance, Accounting, Tax Service, Payroll Manager, Book Keeper, Human Resource.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features six">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-bullhorn fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Sales & Marketing</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Sales, Marketing, Lead Generation, Market Research, Leads.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">


                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features seven">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-wrench fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Engineering & Architecture</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Design, Architect, 3D, CAD, Engineering, Matlab, Electrical, Software.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <a href="services.html">
                        <div class="features eight">
		    <span class="fa-stack fa-3x">
			 <i class="fa fa-circle fa-stack-2x"></i>
			 <i class="fa fa-legal fa-stack-1x fa-inverse"></i>
			</span><!-- /span -->
                            <h4>Legal</h4>
                        </div><!-- /.features -->
                        <div class="spacer">
                            <p>Legal, Paralegal, Contracts, Drafting, Legal Advice, Patents, Federal Law.</p>
                            <p><b>Over 2,000 services</b></p>
                        </div> <!-- /.spacer -->
                    </a>
                </div><!-- /.col-lg-4 -->

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

    </div><!-- /.container -->
</section><!-- /section -->

<!-- ==============================================
Banner Section
=============================================== -->
<section class="banner-comment">
    <div class="container">
        <h1>Best way to help world society is to develop a concrete framework for high paying jobs and individual skills enhancements.</h1>
    </div><!-- /container -->
</section><!-- /section -->




<?php
require_once "inc/footer.php";
?>
