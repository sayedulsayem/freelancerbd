<?php

require_once "inc/header.php";
include_once "lib/session.php";
include_once "lib/user.php";
session::checkSession();

$login=session::get('login');
$loginID=session::get('id');

$user_type=session::get('user_type');
if ($user_type=="client" && $user_type!="admin"){
    header('Location:client_profile.php');
}

?>


<?php

$user=new user();

$userData=$user->getUserDataByID($loginID);

$skill=$user->getSkillList();

$skill_by_user=$user->getSkillById($loginID);

if ($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['skill_add'])){
	$skill_add=$user->add_skill($loginID,$_POST);
}

?>


    <!--carousel starts here-->
<section id="header-login" class="freelancer-profile">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img class="img-thumbnail img-responsive" width="200" height="200" src="inc/img/banner.jpg" alt="">
                <h1><?php echo $userData->user_name; ?></h1>
                <p><i class="fa fa-map-marker"></i><?php echo $userData->sub_dist; ?>, <?php echo $userData->district; ?></p>
                <a class="frlncr-btn frlncr-btn-mint-small" href=""><i class="fa fa-align-left"></i>Get A Quote</a>
            </div>
        </div>
    </div>
</section>
<!--carousel ends here-->
<!--Freelancer Profile starts here-->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-lg-12 white-2">
            <div class="about overview">
                <h3 class="about_us" >About Me</h3>
                <div class="col-lg-12 top-sec">
                    <p><?php echo $userData->about; ?></p>
                    <span style="color:  #000; font-size: 16px;" class="label">Skilles :</span>
                    <?php
                    if (isset($skill_by_user)){
                        foreach ($skill_by_user as $item){?>
                            <span class="label label-success"><?php echo $item['sk_name']; ?></span>
                            <?php
                        }
                    }
                    ?>

                    <span><button onclick="add_skill()">Add Skill</button></span>
                </div>
                <?php
                if (isset($skill_add)){
                    echo $skill_add;
	                sleep(3);
	                header("Refresh:0");
                }
                ?>
                <div id="add_skill" class="add_skill">
                    <form action="" method="post">
                        <div style="padding: 10px;" class="form-group row">
                            <label for="sk_id"> Skill :</label>
                            <select class="form-control custom-form-login" name="sk_id" id="sk_id">
			                    <?php
			                    if (isset($skill)){
				                    foreach ($skill as $item){ ?>
                                        <option value="<?php echo $item['id']?>"><?php echo $item['sk_name']?></option>
					                    <?php
				                    }
			                    }
			                    ?>
                            </select>
                        </div>
                        <button type="submit" name="skill_add" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>

            <div class="row bottom-sec overview bottom-sec">
                <div class="col-lg-12">
                    <hr class="small-hr">

                    <div class="col-lg-2">
                        <h5>Location</h5>
                        <p><i class="fa fa-map-marker"></i>Dhaka</p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Completed</h5>
                        <p>145 Jobs</p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Earns</h5>
                        <p>$1500</p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Ratings</h5>
                        <p><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
                    </div>

                    <div class="col-lg-4">
                        <h5>Learn HTML 5</h5>
                        <p>92%</p>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary progress-bar-mint" role="progressbar" style="width: 80%;" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100">

                            </div>
                        </div>
                    </div>

                    <hr class="small-hr">

                    <div class="col-lg-2">
                        <h5>Work Hour</h5>
                        <p>145</p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Rate/Hour</h5>
                        <p>$15</p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Phone</h5>
                        <p><i class="fa fa-phone"></i><?php echo $userData->mobile; ?></p>
                    </div>

                    <div class="col-lg-2">
                        <h5>Website</h5>
                        <p>www.facebook.com</p>
                    </div>

                    <div class="col-lg-4">
                        <h5>Mail</h5>
                        <p><?php echo $userData->email; ?></p>
                    </div>
                </div>
            </div>

            <div class="skills overview">
                <h3 >Skills</h3>
                <div class="col-md-3 col-sm-6 text-center">
                    <span class="chart easyPieChart " data-percent="90" style="width: 140px; height: 140px; line-height: 140px;">
                        <span class="percent">
                            90
                        </span>
                        <canvas width="280" height="280" style="width: 140px; height: 140px;"></canvas>
                    </span>
                    <h5 class="text-center">Web Design/UI/UX</h5>
                </div>

                <div class="col-md-3 col-sm-6 text-center">
                    <span class="chart easyPieChart " data-percent="90" style="width: 140px; height: 140px; line-height: 140px;">
                        <span class="percent">
                            90
                        </span>
                        <canvas width="280" height="280" style="width: 140px; height: 140px;"></canvas>
                    </span>
                    <h5 class="text-center">Web Design/UI/UX</h5>
                </div>

                <div class="col-md-3 col-sm-6 text-center">
                    <span class="chart easyPieChart " data-percent="90" style="width: 140px; height: 140px; line-height: 140px;">
                        <span class="percent">
                            90
                        </span>
                        <canvas width="280" height="280" style="width: 140px; height: 140px;"></canvas>
                    </span>
                    <h5 class="text-center">Web Design/UI/UX</h5>
                </div>

                <div class="col-md-3 col-sm-6 text-center">
                    <span class="chart easyPieChart " data-percent="90" style="width: 140px; height: 140px; line-height: 140px;">
                        <span class="percent">
                            90
                        </span>
                        <canvas width="280" height="280" style="width: 140px; height: 140px;"></canvas>
                    </span>
                    <h5 class="text-center">Web Design/UI/UX</h5>
                </div>
            </div>
        </div>



    </div>
</div>

<!--Freelancer Profile ends here-->




<?php
require_once "inc/footer.php";
?>