
<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');
session::checkLogin();

$user = new user();

if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['register'])){
	$userReg= $user->userRegister($_POST,"freelancer");
}

?>


                                    <!--carousel starts here-->
<section id="header-login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2> <i class="fa fa-coffee" aria-hidden="true"></i> Register</h2>
                <h4>New Here? Create Account.</h4>
            </div>
        </div>
    </div>
</section>
                                    <!--carousel ends here-->

                                    <!--Signup starts here-->
<section id="signup">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center col-lg-offset-6">
                <h3 class="login-head">Register</h3>
	            <?php
	            if (isset($userReg)){
		            if ($userReg=="1") {
			            echo "<div class='alert alert-success'><strong>Success !</strong> thank you, you have registerd</div>";
			            header("location:freelancer_profile.php");
		            }
		            else{
			            echo $userReg;
		            }
	            }
	            ?>
                <form class="registration" action="" method="post">

                    <div class="form-group row">
                        <label for="userName">User Name :</label>
                        <input type="text" name="userName" class="form-control custom-form-login" id="userName" placeholder="User Name">
                    </div>

                    <div class="form-group row">
                        <label for="firstName">First Name :</label>
                        <input type="text" name="firstName" class="form-control custom-form-login" id="firstName" placeholder="First Name">
                    </div>

                    <div class="form-group row">
                        <label for="lastName">Last Name :</label>
                        <input type="text" name="lastName" class="form-control custom-form-login" id="lastName" placeholder="Last Name">
                    </div>

                    <div class="form-group row">
                        <label for="email">Email : </label>
                        <input type="email" name="email" class="form-control custom-form-login" id="email"
                               placeholder="Email">
                    </div>

                    <div class="form-group row">
                        <label for="password">Password : * </label>
                        <input type="password" name="password" class="form-control custom-form-login" id="password"
                               placeholder="Password">

                    </div>

                    <div class="form-group row">
                        <label for="cPassword">Re-enter Password : * </label>
                        <input type="password" name="cPassword" class="form-control custom-form-login" id="cPassword"
                               placeholder="Confirm Password">

                    </div>

                    <div class="form-group row">
                        <label for="mobile">Mobile No : *</label>
                        <input type="number" name="mobile" class="form-control custom-form-login" id="mobile"
                               placeholder="Mobile Number">
                    </div>

                    <div class="form-group row">
                        <label for="about">About Yourself :</label>
                        <textarea name="about" class="form-control textarea" id="about"
                                  placeholder="About" rows="8" cols="3"></textarea>
                    </div>
                    <h3 class="address">Address</h3>
                    <div class="form-group row">
                        <label for="division">Division :</label>
                        <select class="form-control custom-form-login" name="division" id="division">
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                        </select>
                    </div>

                    <div class="form-group row">
                        <label for="district">District :</label>
                        <select class="form-control custom-form-login" name="district" id="district">
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                        </select>
                    </div>

                    <div class="form-group row">
                        <label for="subdist">Subdistrict :</label>
                        <select class="form-control custom-form-login" name="subdist" id="subdist">
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                        </select>
                    </div>

                    <div class="form-group row">
                        <label for="zipCode">Zip Code :</label>
                        <select class="form-control custom-form-login" name="zipCode" id="zipCode">
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                            <option value="dhaka">Dhaka</option>
                        </select>
                    </div>

                    <div class="form-group row">
                        <label for="houseNo">House No :</label>
                        <input type="number" name="houseNo" class="form-control custom-form-login" id="houseNo"
                               placeholder="House Number">
                    </div>

                    <div class="form-group row">
                        <label for="roadN">Road No :</label>
                        <input type="text" name="roadN" class="form-control custom-form-login" id="roadN"
                               placeholder="Road Name/Number">
                    </div>

                    <button type="submit" name="register" class="btn login-button">Register</button>

                </form>
            </div>

        </div>
    </div>
</section>
                                    <!--Signup ends here-->

<?php
require_once "inc/footer.php";
?>