<?php

require_once "inc/header.php";
include_once "lib/session.php";
include_once "lib/user.php";
session::checkSession();

$login=session::get('login');
$loginID=session::get('id');

$user_type=session::get('user_type');

if ($user_type=="freelancer" && $user_type!="admin"){
	header('Location:freelancer_profile.php');
}
elseif ($user_type=="client" && $user_type!="admin"){
	header('Location:client_profile.php');
}

?>

<?php
require_once "inc/footer.php";
?>
