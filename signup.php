<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');

?>

                                    <!--carousel starts here-->
<section id="header-login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2> <i class="fa fa-coffee" aria-hidden="true"></i> Sign Up</h2>
                <h4>first, tell us what you're looking for</h4>
            </div>
        </div>
    </div>
</section>
                                    <!--carousel ends here-->
                                    <!--Signup starts here-->
<section id="signup">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text-center">
                <i class="signup-icon fa fa-user-circle" aria-hidden="true"></i>
                <h4>I want to hire a freelancer</h4>
                <p>Find, collaborate with, and pay an expert.</p>
                <a href="client_registration.php"><button type="button" class="btn btn-success">Hire</button></a>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 divider text-center">
                <span class="or">
                    or
                </span>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text-center">
                <i class="signup-icon fa fa-user-circle" aria-hidden="true"></i>
                <h4>I am looking for online work.</h4>
                <p>Find freelance projects and grow your business.</p>
                <a href="freelancer_registration.php"><button type="button" class="btn btn-success">Work</button></a>
            </div>

        </div>
    </div>
</section>
                                    <!--Signup ends here-->




<?php
require_once "inc/footer.php";
?>