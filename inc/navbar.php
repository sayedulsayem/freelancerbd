<?php

$filepath= realpath(dirname(__FILE__));
include_once $filepath."/../admin/lib/session.php";
include_once $filepath."/../lib/user.php";

$login=session::get('login');
$loginID=session::get('id');
$user_type=session::get('user_type');

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);

?>

<!--navbar starts from here-->
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light nav-bg-color bg-dark" role="navigation">
	<a class="navbar-brand" href="index.php">
		<i class="fa fa-coffee" aria-hidden="true"></i>
		Freelancer</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="nav navbar-nav mr-auto sa-margin">
			<li class="nav-item <?php echo $active = ($basename == 'index') ? ' active' : ''; ?>" id="index">
				<a class="nav-link" href="index.php">Home</a>
			</li>
			<li class="nav-item <?php echo $active = ($basename == 'about') ? ' active' : ''; ?>"
			    id="about">
				<a class="nav-link" href="about.php">About</a>
			</li>
			<li class="nav-item dropdown <?php echo $active = ($basename == 'jobs' || $basename== 'post_a_job') ? ' active' : '';
			?>">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Jobs
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item <?php echo $active = ($basename == 'jobs') ? ' active' : ''; ?>" href="jobs
					.php">Jobs</a>
                    <?php
                    if ($user_type=="client"){
                        ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item <?php echo $active = ($basename == 'post_a_job') ? ' active' : ''; ?>" href="post_a_job.php">Post Job</a>
                    <?php
                    }
                    ?>
				</div>
			</li>
			<li class="nav-item dropdown <?php echo $active = ($basename == 'service') ? ' active' : ''; ?>">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Freelancers
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item <?php echo $active = ($basename == 'service') ? ' active' : ''; ?>" href="service.php">Services</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="#">Service Page</a>
				</div>
			</li>
			<li class="nav-item <?php echo $active = ($basename == 'how_it_works') ? ' active' : ''; ?>">
				<a class="nav-link" href="how_it_works.php">How It Works</a>
			</li>
			<li class="nav-item dropdown <?php echo $active = ($basename == 'login' || $basename== 'signup' || $basename== 'freelancer_registration' || $basename=='client_registration') ? ' active' : ''; ?>">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Sign In
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item <?php echo $active = ($basename == 'login') ? ' active' : ''; ?>" href="login
					.php">Login</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item <?php echo $active = ($basename == 'signup') ? ' active' : ''; ?>" href="signup.php">Register</a>
				</div>
			</li>
			<li class="nav-item <?php echo $active = ($basename == 'contact') ? ' active' : ''; ?>">
				<a class="nav-link" href="contact.php">Contact</a>
			</li>
			<li class="nav-item <?php echo $active = ($basename == 'fag') ? ' active' : ''; ?>">
				<a class="nav-link" href="faq.php">Faq</a>
			</li>

			<?php
			if ($login==true && $user_type!="admin"){ ?>

				<li class="nav-item <?php echo $active = ($basename == 'profile' || $basename=='client_profile' || $basename=='freelancer_profile') ? ' active' : ''; ?>">
					<a class="nav-link" href="profile.php">Profile</a>
				</li>

				<?php
			}
			?>

			<?php
			if ($login==true && $user_type!="admin"){ ?>

				<li class="nav-item <?php echo $active = ($basename == 'log_out') ? ' active' : ''; ?>">
					<a class="nav-link" href="log_out.php">log Out</a>
				</li>

				<?php
			}
			?>
		</ul>

	</div>
</nav>
<!--navbar ends here-->