<?php

require_once "inc/header.php";
include_once "lib/user.php";

$login=session::get('login');
$loginID=session::get('id');
$user_type=session::get('user_type');

if ($login!=true){
    header("Location:login.php");
} elseif ($user_type!="client"){
	header("Location:index.php");
}

$user=new user();

$cat=$user->getCatList();
$skill=$user->getSkillList();

if ($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['post_job'])){
    $post_job=$user->postJob($loginID,$_POST);
}


?>

<!--carousel starts here-->
<section id="header-login">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2> <i class="fa fa-coffee" aria-hidden="true"></i> Post a job</h2>
				<h4>post a job here. so that freelancer can send you work proposal. </h4>
			</div>
		</div>
	</div>
</section>
<!--carousel ends here-->

<!--Signup starts here-->
<section id="job_post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center col-lg-offset-6">

                <?php
                if (isset($post_job)){
                    echo $post_job;
                }
                ?>

				<form class="registration" action="" method="post">

					<div class="form-group row">
						<label for="job_title">Job Title :</label>
						<input type="text" name="job_title" class="form-control custom-form-login" id="job_title"
						       placeholder="Enter Here Your Job Title">
					</div>

					<div class="form-group row">
						<label for="cat_id">Select Job Category :</label>
						<select class="form-control custom-form-login" name="cat_id" id="cat_id">
							<?php
							if (isset($cat)){
								foreach ($cat as $value){ ?>
									<option value="<?php echo $value['id']?>"><?php echo $value['cat_name']?></option>
									<?php
								}
							}
							?>
						</select>
					</div>

					<div class="form-group row">
						<label for="sk_id">Select Needed Skill :</label>
						<select class="form-control custom-form-login" name="sk_id" id="sk_id">
							<?php
							if (isset($skill)){
								foreach ($skill as $item){ ?>
									<option value="<?php echo $item['id']?>"><?php echo $item['sk_name']?></option>
									<?php
								}
							}
							?>
						</select>
					</div>

					<div class="form-group row">
						<label for="job_desc">Job Description :</label>
						<textarea name="job_desc" class="form-control textarea" id="job_desc"
						          placeholder="Job Description" rows="8" cols="3"></textarea>
					</div>

					<div class="form-group row">
						<label for="payment">Payment :</label>
						<input type="text" name="payment" class="form-control custom-form-login" id="payment"
						       placeholder="Payment">
					</div>

					<div class="form-group row">
						<label for="date">Date :</label>
						<input type="date" name="date" class="form-control custom-form-login" id="date">
					</div>

					<div class="form-group row">
						<label for="start_time">Start Time :</label>
						<input type="time" name="start_time" class="form-control custom-form-login" id="start_time">
					</div>

					<div class="form-group row">
						<label for="end_time">End Time :</label>
						<input type="time" name="end_time" class="form-control custom-form-login" id="end_time">
					</div>

					<button type="submit" name="post_job" class="btn login-button">Post Job</button>

				</form>
			</div>

		</div>
	</div>
</section>
<!--Signup ends here-->

<?php
require_once "inc/footer.php";
?>
