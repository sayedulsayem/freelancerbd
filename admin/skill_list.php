<?php
require_once "dist/header.php";
include_once "lib/admin.php";
session::checkSession();

$admin=new admin();

$sk_list=$admin->getSkillList();

?>

	<div class="content-wrapper">
		<section class="content">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Skill List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Skill ID</th>
                            <th>Skill Name</th>
                            <th>Category Name</th>
                            <th>Skill Description</th>
                        </tr>
                        </thead>
						<?php
						if (isset($sk_list)){
							foreach ($sk_list as $item){
								?>
                                <tbody>
                                <tr>
                                    <td><?php echo $item['id']; ?></td>
                                    <td><?php echo $item['sk_name']; ?></td>
                                    <td><?php echo $item['cat_name']; ?></td>
                                    <td><?php echo $item['sk_desc']; ?></td>
                                </tr>
                                </tbody>
								<?php
							}
						}
						?>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>


		</section>
	</div>

<?php
require_once "dist/footer.php";
?>