<?php
require_once "dist/header.php";
include_once "lib/admin.php";
session::checkSession();

$admin=new admin();

$cat_list=$admin->getCatList();

if ($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['sk_submit'])){
    $add_sk=$admin->addSkill($_POST);
}
?>

	<div class="content-wrapper">
		<section class="content">

            <h1>Add New Skill</h1>

            <?php
            if (isset($add_sk)){
                echo $add_sk;
            }
            ?>

            <form role="form" action="" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="sk_name">Skill Name</label>
                        <input name="sk_name" type="text" class="form-control" id="sk_name" placeholder="Enter a skill name">
                    </div>

                    <div class="form-group">
                        <label for="cat_id">Select Category</label>
                        <select class="form-control" name="cat_id" id="division">

                            <?php
                            if (isset($cat_list)){
                                foreach ($cat_list as $item){
                                    ?>
                                    <option value="<?php echo $item['id']; ?>"><?php echo $item['cat_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>

                    <div class="form-group">
                        <label style="display: block" for="sk_desc">Skill Description</label>
                        <textarea name="sk_desc" id="sk_desc" cols="187" rows="10"></textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <button name="sk_submit" type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

		</section>
	</div>

<?php
require_once "dist/footer.php";
?>