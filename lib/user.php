<?php

include_once "database.php";
$filepath= realpath(dirname(__FILE__));
include_once $filepath."/../admin/lib/session.php";
session::init();

class user{

	private $db;
	public function __construct()
	{
		$this->db=new database();
	}

	public function emailCheck($email){

		$email=$email;

		$sql="SELECT email FROM users WHERE email=:email";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':email',$email);
		$query->execute();
		if($query->rowCount()>0){
			return true;
		}
		else{
			return false;
		}
	}

	public function mobileCheck($mobile){

		$sql="SELECT mobile FROM users WHERE mobile=:mobile";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':mobile',$mobile);
		$query->execute();
		if($query->rowCount()>0){
			return true;
		}
		else{
			return false;
		}
	}

	public function userRegister($data,$user_type){

		$userName=$data['userName'];
		$firstName=$data['firstName'];
		$lastName=$data['lastName'];
		$email=$data['email'];
		$pass=$data['password'];
		$cPass=$data['cPassword'];
		$password=md5($pass);
		$mobile=$data['mobile'];
		$about=$data['about'];
		$division=$data['division'];
		$district=$data['district'];
		$subdist=$data['subdist'];
		$zipCode=$data['zipCode'];
		$houseNo=$data['houseNo'];
		$roadN=$data['roadN'];

		$chk_email=$this->emailCheck($email);
		$chk_mobile=$this->mobileCheck($mobile);

		if($userName=="" OR $email=="" OR $pass=="" OR $mobile== "" OR $zipCode== ""){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Field must not be empty</div>";
			return $msg;
		}
		if(strlen($pass) < 5){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Password must be at least 5 Characters</div>";
			return $msg;
		}
		elseif (preg_match('/[^a-z0-9_-]+/i', $pass)){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Characters must be a-z,0-9,_,-</div>";
			return $msg;
		}
		if (filter_var($email, FILTER_VALIDATE_EMAIL)=== false){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Enter a valid email address</div>";
			return $msg;
		}
		if ($chk_email==true){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> this admin email already registered </div>";
			return $msg;
		}
		if ($chk_mobile==true){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> this mobile number already registered </div>";
			return $msg;
		}

		if ($pass!=$cPass){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> password doesn't matched </div>";
			return $msg;
		}

		$sql="INSERT INTO `users` (`user_name`, `first_name`, `last_name`, `email`, `password`, `mobile`, `about`, `user_type`) VALUES (:user_name, :first_name, :last_name, :email, :password, :mobile, :about, :user_type);";

		$query=$this->db->pdo->prepare($sql);

		$query->bindValue(':user_name',$userName);
		$query->bindValue(':first_name',$firstName);
		$query->bindValue(':last_name',$lastName);
		$query->bindValue(':email',$email);
		$query->bindValue(':password',$password);
		$query->bindValue(':mobile',$mobile);
		$query->bindValue(':about',$about);
		$query->bindValue(':user_type',$user_type);

		$sqlAddress="INSERT INTO `addresses` (`mobile`, `division`, `district`, `sub_dist`, `zipcode`, `house_no`, `road_no`) VALUES (:mobile, :division, :district, :sub_dist, :zipcode, :house_no, :road_no);";

		$queryaddress=$this->db->pdo->prepare($sqlAddress);

		$queryaddress->bindValue(':mobile',$mobile);
		$queryaddress->bindValue(':division',$division);
		$queryaddress->bindValue(':district',$district);
		$queryaddress->bindValue(':sub_dist',$subdist);
		$queryaddress->bindValue(':zipcode',$zipCode);
		$queryaddress->bindValue(':house_no',$houseNo);
		$queryaddress->bindValue(':road_no',$roadN);

		$result1=$query->execute();
		$result2=$queryaddress->execute();

		if ($result1 && $result2){
			$msg ="1";
			return $msg;
		}
		else{
			$msg ="<div class='alert alert-danger'><strong>Error !</strong> try again </div>";
			return $msg;
		}

	}

	public function getLoginUser($email,$password){

		$sql= "SELECT * FROM `users` WHERE email = :email AND password = :password LIMIT 1;";

		$query=$this->db->pdo->prepare($sql);

		$query->bindValue(':email',$email);

		$query->bindValue(':password',$password);

		$query->execute();

		$result=$query->fetch(PDO::FETCH_OBJ);

		return $result;

	}

	public function userLogin($data){
		$email=$data['email'];
		$pass=$data['password'];
		$password=md5($pass);

		$chk_email=$this->emailCheck($email);

		if($email=="" OR $pass== ""){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Field must not be empty</div>";
			return $msg;
		}
		if (filter_var($email, FILTER_VALIDATE_EMAIL)=== false){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Enter a valid email address</div>";
			return $msg;
		}
		if ($chk_email==false){
			$msg="<div class='alert alert-danger'><strong>Error !</strong> this email address does not exist</div>";
			return $msg;
		}

		$result= $this->getLoginUser($email,$password);

		if ($result){

			session::init();
			session::set("login",true);
			session::set("id",$result->id);
			session::set("user_name",$result->user_name);
			session::set("email",$result->email);
			session::set("user_type",$result->user_type);
			session::set("loginmsg","<div class='alert alert-success'><strong>Success !</strong> You are loggedIn</div>");
			header('Location:index.php');
			$msg="<div class='alert alert-success'><strong>Success !</strong> thank you, you have successfully logged in</div>";
			return $msg;
		}
		else{
			$msg="<div class='alert alert-danger'><strong>Error !</strong> Email and password doesn't match</div>";
			return $msg;
		}

	}

	public function getUserDataByID($reqID){
		$userId=$reqID;
		$sql= "SELECT * FROM `users`,`addresses` WHERE users.id= :id AND users.mobile=addresses.mobile LIMIT 1";
		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':id',$userId);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function getCatList(){

		$sql="SELECT * FROM `categories` ORDER BY id DESC;";
		$query=$this->db->pdo->prepare($sql);
		$query->execute();
		$result=$query->fetchAll();

		return $result;

	}

	public function getSkillList(){

		$sql="SELECT skilles.id,cat_id,cat_name,sk_name,sk_desc FROM `skilles`,`categories` WHERE skilles.cat_id=categories.id ORDER BY skilles
.id DESC;";
		$query=$this->db->pdo->prepare($sql);
		$query->execute();
		$result=$query->fetchAll();

		return $result;

	}

	public function postJob($user_id,$data){

		$id=$user_id;
		$cat_id=$data['cat_id'];
		$job_title=$data['job_title'];
		$sk_id=$data['sk_id'];
		$job_desc=$data['job_desc'];
		$payment=$data['payment'];
		$date=$data['date'];
		$start_time=$data['start_time'];
		$end_time=$data['end_time'];

		$sql="INSERT INTO `job_posts` (`user_id`, `cat_id`, `sk_id`, `job_title`, `job_desc`, `payment`, `date`, `start_time`, `end_time`) 
VALUES (:id, :cat_id, :sk_id, :job_title, :job_desc, :payment, :date_p, :start_time, :end_time);";

		$query=$this->db->pdo->prepare($sql);
		$query->bindValue(':id',$id);
		$query->bindValue('cat_id',$cat_id);
		$query->bindValue('sk_id',$sk_id);
		$query->bindValue('job_title',$job_title);
		$query->bindValue('job_desc',$job_desc);
		$query->bindValue('payment',$payment);
		$query->bindValue('date_p',$date);
		$query->bindValue('start_time',$start_time);
		$query->bindValue('end_time',$end_time);

		$result=$query->execute();

		if ($result){
			$msg="<div class='alert alert-success'><strong>Success !</strong> Thank you, you have successfully posted a job. </div>";
			return $msg;
		}else{
			$msg="<div class='alert alert-danger'><strong>Failed !</strong> Can't posted, try again. </div>";
			return $msg;
		}

	}

	public function getAllJobs(){
	    $sql="SELECT * FROM job_posts,users,addresses,skilles,categories WHERE job_posts.user_id=users.id AND users.mobile=addresses.mobile AND job_posts.cat_id=categories.id AND job_posts.sk_id=skilles.id ORDER BY job_posts.id DESC;";
	    $query=$this->db->pdo->prepare($sql);
        $query->execute();
	    $result=$query->fetchAll();
	    return $result;
    }

    public function add_skill($user_id,$data){
		$id=$user_id;
		$skill_id=$data['sk_id'];

	    $sql = "INSERT INTO `freelancer_skills` (`user_id`, `sk_id`) VALUES ( :user_id, :sk_id);";
	    $query=$this->db->pdo->prepare($sql);
	    $query->bindValue(':user_id',$id);
	    $query->bindValue(":sk_id",$skill_id);

	    $result=$query->execute();

	    if ($result){
		    $msg="<div class='alert alert-success'><strong>Success !</strong> Thank you, you have successfully added skill. </div>";
		    return $msg;
	    }else{
		    $msg="<div class='alert alert-danger'><strong>Failed !</strong> Can't added skill, try again. </div>";
		    return $msg;
	    }
    }

    public function getSkillById($user_id){
	    $sql = "SELECT * FROM freelancer_skills,skilles WHERE freelancer_skills.user_id= :user_id AND freelancer_skills.sk_id=skilles.id";
	    $query=$this->db->pdo->prepare($sql);
	    $query->bindValue(':user_id',$user_id);
	    $query->execute();
	    $result=$query->fetchAll();
	    return $result;
    }


}
?>